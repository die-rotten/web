import cookie from 'js-cookie'

export function set (key, value) {
  return cookie.set(key, value)
}

export function get (key) {
  return cookie.get(key)
}

export function remove (key) {
  return cookie.remove(key)
}
